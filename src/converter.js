const pad = (hex) => {
    return (hex.length === 1 ? "0" + hex : hex)
}

const padb = (rgb) => {
    return (rgb < 10 ? "0" + rgb : " " + rgb)
}

module.exports = {
    rgbToHex: (red, green, blue) => {
        const redHex = red.toString(16);
        const greenHex = green.toString(16);
        const blueHex = blue.toString(16);
        
        return  pad(redHex) + pad(greenHex) + pad(blueHex); //ff0000
    } ,
    hexToRGB: (rd, gr, bl) => {
        redDec = padb(parseInt(rd, 16));
        greenDec = padb(parseInt(gr, 16));
        blueDec = padb(parseInt(bl, 16));

        redTrim = redDec.trim();
        greenTrim = greenDec.trim();
        blueTrim = blueDec.trim();

        const a = [redTrim, greenTrim, blueTrim]; 
        const b = a.join(" ");
        const c = b.trim();
        const d = c.replace(" ", "");
        const str = d.replace(" ", "");
        console.log("RGB", str); 

        return  str;
    }
}