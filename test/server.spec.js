// ./test/server.spec.js - Integration test
const expect = require("chai").expect;
const request = require("request");
const app = require("../src/server");
const port = 3000;

describe("Color Code Converter API", () => {
    let server = undefined; // works without defining
    before("Start server before run tests", (done) => {
        server = app.listen(port, () => {
            console.log(`Server listening: localhost:${port}`);
            done();
        });
    });

    // RGB to hex 
    describe("RGB to Hex conversion", () => {
        const urla = `http://localhost:${port}/rgb-to-hex?red=255&green=255&blue=255`;

        it("returns status 200", (done) => {
            request(urla, (error, response, body) => {
                expect(response.statusCode).to.equal(200);
                done();
            });
        });
        it("returns the color in hex", (done) => {
            request(urla, (error, response, body) => {
                expect(body).to.equal("ffffff");
                done();
            });
        });

        // Hex to RGB
        describe("hex to rgb conversion", () => {
            const urlb = `http://localhost:${port}/hex-to-rgb?red=ff&green=00&blue=00`;
    
            it("returns status 200", (done) => {
                request(urlb, (error, response, body) => {
                    expect(response.statusCode).to.equal(200);
                    done();
                });
            });
            it("returns the color in dec", (done) => {
                request(urlb, (error, response, body) => {
                    expect(body).to.equal("2550000");
                    done();
                });
            });
        });
        
    });

    after("Stop server after tests", (done) => {
        server.close();
        done();
    });
});    
