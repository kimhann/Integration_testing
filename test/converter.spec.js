// TDD - Test Driven Development - Unit testing

const expect = require('chai').expect;
const converter = require('../src/converter');



describe("color code converter", () => {
    describe("RGB to HEX conversion", () => {
        it("converts the basic colors", () => {
            const redHex = converter.rgbToHex(255,0,0); // ff0000
            const greenHex = converter.rgbToHex(0,255,0); //00ff00
            const blueHex = converter.rgbToHex(0,0,255); //0000ff

            expect(redHex).to.equal("ff0000");
            expect(greenHex).to.equal("00ff00");
            expect(blueHex).to.equal("0000ff");
        })
    })
    describe("HEX to RGB conversion", () => {
        it("converts the basic colors", () => {
            const redDec = converter.hexToRGB("ff", "00", "00"); 
            const greenDec = converter.hexToRGB("00", "ff", "00"); 
            const blueDec = converter.hexToRGB("00", "00", "ff"); 

            expect(redDec).to.equal("2550000");
            expect(greenDec).to.equal("0025500");
            expect(blueDec).to.equal("0000255");
            console.log("redDec", redDec);
            console.log("greenDec", greenDec);
            console.log("blueDec", blueDec);
        })
    })
})
